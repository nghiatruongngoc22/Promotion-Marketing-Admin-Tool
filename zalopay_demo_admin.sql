-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 05, 2018 at 05:21 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.1.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zalopay_demo_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

CREATE TABLE `merchants` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `code`, `created_date`, `is_active`, `name`) VALUES
(1, 'ADMIN', '2018-07-03 22:03:52', b'1', 'ALL'),
(2, 'TEST', '2018-07-03 22:04:04', b'1', 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `allows` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `denies` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `allows`, `code`, `denies`, `name`) VALUES
(1, '/abc,/def', 'ADM', '/123,/456', 'Admin'),
(2, '/testabc,/testdef', 'TEST', '/test123,/test456', 'TEST');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `created_date`, `display_name`, `email`, `is_active`, `password`, `phone`, `user_name`) VALUES
(1, '2018-07-04 10:13:01', 'An Nguyen Dinh', 'annd@vng.com.vn', b'1', '1234', '01267377251', 'AnND');

-- --------------------------------------------------------

--
-- Table structure for table `user_merchants`
--

CREATE TABLE `user_merchants` (
  `id` int(11) NOT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_merchants`
--

INSERT INTO `user_merchants` (`id`, `permission`, `role`, `merchant_id`, `user_id`) VALUES
(1, '/role,/merchants', '[{\"id\":1,\"name\":\"Admin\",\"code\":\"ADM\",\"allows\":\"/abc,/def\",\"denies\":\"/123,/456\"}]', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_CODE` (`code`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_CODE` (`code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_USER_NAME` (`user_name`),
  ADD UNIQUE KEY `UNIQUE_EMAIL` (`email`);

--
-- Indexes for table `user_merchants`
--
ALTER TABLE `user_merchants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_USER_MERCHANT` (`user_id`,`merchant_id`),
  ADD KEY `FKs4s21y8d5t2kxkcjj1j312jb` (`merchant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_merchants`
--
ALTER TABLE `user_merchants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_merchants`
--
ALTER TABLE `user_merchants`
  ADD CONSTRAINT `FKgvqp66gqta376h0qbg6sygf8k` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FKs4s21y8d5t2kxkcjj1j312jb` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
