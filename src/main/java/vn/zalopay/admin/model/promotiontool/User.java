package vn.zalopay.admin.model.promotiontool;

import com.fasterxml.jackson.core.type.TypeReference;
import vn.zalopay.admin.representation.RoleDTO;
import vn.zalopay.admin.sharedkernel.DataUtils;

import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import vn.zalopay.admin.model.promotiontool.UserMerchant;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "display_name")
    private String displayName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "phone")
    private String phone;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "created_date")
    private Date createdDate;
    
//    @OneToMany(mappedBy="cart")
//    private List<UserMerchant> userMerchants;

    protected User() {
    }

    public User(String userName, String displayName, String email,
            String password, String phone, Boolean isActive,
            List<UserMerchant> userMerchants) {
        this.userName = userName;
        this.displayName = displayName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.isActive = isActive;
        this.createdDate = new Date();

        if (userMerchants != null) {
            merchants().addAll(userMerchants);
        }
    }

    public void updateBasicInfo(String userName, String displayName, String email,
            String password, String phone, Boolean isActive) {
        this.userName = userName;
        this.displayName = displayName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Boolean getActive() {
        return isActive;
    }

    // ==========================================================================
    // User Merchant
    // ==========================================================================
    public void removeMerchant(Integer merchantId) {
        merchants().removeIf(e -> e.getMerchant().getId().equals(merchantId));
    }

    public UserMerchant getMerchant(Integer id) {

        List<UserMerchant> matched = this.merchants()
                .stream()
                .filter(i -> Objects.equals(i.getMerchant().getId(), id))
                .collect(Collectors.toList());

        if (matched.size() == 0) {
            return null;
        }

        return matched.get(0);
    }

    public void addMerchant(List<UserMerchant> userMerchants) {
        merchants().addAll(userMerchants);
    }

    public List<UserMerchant> merchants() {
//        if (this.userMerchants == null) {
//            this.userMerchants = new ArrayList<>();
//        }
        return new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
