package vn.zalopay.admin.model.promotiontool;

import com.fasterxml.jackson.core.type.TypeReference;
import vn.zalopay.admin.representation.UserValidationDTO;
import vn.zalopay.admin.sharedkernel.DataUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import vn.zalopay.admin.model.promotiontool.Role;

public class UserMerchant {

    private Integer id;
    private Merchant merchant;
    private String role;
    private String permission;

    protected UserMerchant() {}

    public UserMerchant(String role, String permission, Merchant merchant) {
        this.role = role;
        this.permission = permission;
        this.merchant = merchant;
    }

    public void update(String role, String permission) {
        this.role = role;
        this.permission = permission;
    }

    public Integer key() {
        return id;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public String getRole() {
        return role;
    }

    public String getPermission() {
        return permission;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public UserValidationDTO checkAllowAccessAndReturnData(String path) {

        UserValidationDTO userValidationDTO = new UserValidationDTO();

        List<Role> roles = DataUtils.parseData(this.getRole(), new TypeReference<List<Role>>(){} );

        Set<String> allowPaths = new HashSet<>(Arrays.asList(this.getPermission().split(",")));
        Set<String> deniPaths = new HashSet<>();

        if (roles != null) {
            roles.forEach(role -> {
                allowPaths.addAll(Arrays.asList(role.getAllows().split(",")));
                deniPaths.addAll(Arrays.asList(role.getDenies().split(",")));
            });
        }

        userValidationDTO.setAllowPaths(allowPaths);
        userValidationDTO.setDeniPaths(deniPaths);
        userValidationDTO.setIsValid(false);


        for (String denyPath : deniPaths) {
            if (denyPath.contains(path)) {
                return userValidationDTO;
            }
        }

        for (String allowPath : allowPaths) {

            if (allowPath.equals("/")) {
                userValidationDTO.setIsValid(true);
                return userValidationDTO;
            }

            if (allowPath.length() > path.length()) {
                if (allowPath.contains(path)) {
                    userValidationDTO.setIsValid(true);
                    return userValidationDTO;
                }
            } else {
                if (path.contains(allowPath)) {
                    userValidationDTO.setIsValid(true);
                    return userValidationDTO;
                }
            }
        }
        return userValidationDTO;
    }

}