package vn.zalopay.admin.model.promotiontool;

import vn.zalopay.admin.model.promotiontool.Role;


public interface RoleRepository {

    void save(Role role);

    Role get(Integer id);

    Role getByCode(String code);

}