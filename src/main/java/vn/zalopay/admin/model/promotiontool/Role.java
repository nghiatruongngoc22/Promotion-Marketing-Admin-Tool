package vn.zalopay.admin.model.promotiontool;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role {

    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;
    @Column(name = "allows")
    private String allows;
    @Column(name = "denies")
    private String denies;

    protected Role() {
    }

    public Role(String name, String code, String allows, String denies) {
        this.name = name;
        this.code = code;
        this.allows = allows;
        this.denies = denies;
    }

    public void updateRole(String name, String code, String allows, String denies) {
        this.name = name;
        this.code = code;
        this.allows = allows;
        this.denies = denies;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getAllows() {
        return allows;
    }

    public String getDenies() {
        return denies;
    }
}
