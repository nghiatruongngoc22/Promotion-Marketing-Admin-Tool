package vn.zalopay.admin.model.promotiontool;


public interface UserRepository {

    void save(User user);

    void delete(User user);

    User get(Integer id);

}