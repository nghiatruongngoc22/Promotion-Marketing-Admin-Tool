package vn.zalopay.admin.model.promotiontool;


public interface MerchantRepository {

    void save(Merchant merchant);

    Merchant get(Integer id);

}