package vn.zalopay.admin.query;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class AbstractQuerySpecification<T> implements Specification<T> {

    protected List<String> filters;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * Construct new query specification with a {@link List list} of filter strings
     * @param filters An array of filter string
     */
    public AbstractQuerySpecification(List<String> filters) {
        if (filters == null) {
            this.filters = new ArrayList<>();
        }
        else {
            this.filters = filters;
        }
    }

    /**
     * Construct new query specification with an array of filter strings
     * @param filters An array of filter string
     */
    public AbstractQuerySpecification(String[] filters) {
        if (filters != null) {
            this.filters = new ArrayList<>();
            Collections.addAll(this.filters, filters);
        }
        else {
            this.filters = new ArrayList<>();
        }
    }

    /**
     * Convert to {@link Predicate}
     * @param root The root type in the from clause
     * @param criteriaQuery The criteria query
     * @param builder The criteria query builder
     * @return The predicate
     */
    @SuppressWarnings("unchecked")
    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
        buildOrder(root, criteriaQuery, builder);

        List<Predicate> predicateList = new ArrayList<>();

        for (String filter : filters) {
            Pattern pattern = Pattern.compile("([^=><(~=)(!?\\{\\})]+)(=|~=|>=|<=|>|<|\\{\\}|!\\{\\})(.+)");
            Matcher matcher = pattern.matcher(filter);
            if (!matcher.find()) {
                continue;
            }

            String name = matcher.group(1);
            String operator = matcher.group(2);
            String value = matcher.group(3);
            Path path;

            if (!isSupported(name)) {
                throw new IllegalArgumentException("Unsupported filter: '" + name + "'");
            }

            switch (operator) {
                case "~=":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(builder.like(path, String.valueOf(convert(path, value)) + "%"));
                    break;
                case "=":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(builder.equal(path, convert(path, value)));
                    break;
                case ">":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(builder.greaterThan(path, (Comparable) convert(path, value)));
                    break;
                case ">=":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(builder.greaterThanOrEqualTo(path, (Comparable) convert(path, value)));
                    break;
                case "<":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(builder.lessThan(path, (Comparable) convert(path, value)));
                    break;
                case "<=":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(builder.lessThanOrEqualTo(path, (Comparable) convert(path, value)));
                    break;
                case "{}":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(path.in(Arrays.asList(value.split(";"))));
                    break;
                case "!{}":
                    path = resolvePath(root, getRealAttributeName(name));
                    predicateList.add(path.in(convert(path, Arrays.asList(value.split(";")))).not());
                    break;
                default:
                    break;
            }
        }

        Predicate[] predicates = new Predicate[predicateList.size()];
        int i = 0;
        for (Predicate p : predicateList) {
            predicates[i] = p;
            i++;
        }

        return builder.and(predicates);
    }

    /**
     * Whether given filter name is supported or not
     * @param filterName The name of filter
     * @return {@code true} if filter name is supported, otherwise {@code false}
     */
    private boolean isSupported(String filterName) {
        return supportedFilters().contains(filterName);
    }

    /**
     * Convert a value to a value which is accepted by given JPA criteria path
     * @param path The JPA criteria path to attribute
     * @param value The value to be converted
     * @return The converted value or {@code null} if value is not acceptable
     */
    @SuppressWarnings("unchecked")
    private Object convert(Path path, Object value) {
        if (path.getJavaType().isEnum()) {
            if (value instanceof String) {
                //noinspection unchecked
                try {
                    return path.getJavaType().getMethod("valueOf", String.class).invoke(null, value);
                } catch (Exception e) {
                    return null;
                }
            } else if (value instanceof Collection) {
                return ((Collection) value)
                        .stream()
                        .map(e -> {
                            try {
                                return path.getJavaType()
                                        .getMethod("valueOf", String.class)
                                        .invoke(null, e);
                            } catch (Exception ignored) {
                                return null;
                            }
                        })
                        .collect(Collectors.toList()) ;
            }

            return null;
        }
        else if (Date.class == path.getJavaType()) {
            if (String.class.isInstance(value)) {
                LocalDateTime dateTime = LocalDateTime.from(dateFormatter().parse((String) value));
                return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
            }
        }

        return value;
    }

    private DateTimeFormatter dateFormatter() {
        return this.dateTimeFormatter;
    }

    private Path resolvePath(Root<T> root, String name) {
        if (!name.contains(".")) {
            return root.get(name);
        }

        Path result = null;
        String[] pathName = name.split("\\.");
        for (String path : pathName) {
            if (result == null) {
                result = root.get(path);
            }
            else {
                result = result.get(path);
            }
        }

        return result;
    }

    private String getRealAttributeName(String name) {
        return attributeMap().get(name.trim());
    }

    private Set<String> supportedFilters() {
        return attributeMap().keySet();
    }

    /**
     * @return A from with client submitted filter name as key and the
     *         real JPA property name as value.
     */
    protected abstract Map<String, String> attributeMap();

    /**
     * Construct the order of query specification
     * @param root The root entity
     * @param criteriaQuery The query
     * @param builder The criteria builder
     */
    protected abstract void buildOrder(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder);

    /**
     * Set date time formatter to convert date time in string representational to {@link Date}
     * @param dateTimeFormatter The date time formatter
     */
    public void setDateTimeFormatter(DateTimeFormatter dateTimeFormatter) {
        this.dateTimeFormatter = dateTimeFormatter;
    }
}
