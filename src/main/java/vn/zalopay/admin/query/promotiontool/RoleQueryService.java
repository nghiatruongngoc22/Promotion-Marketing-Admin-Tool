package vn.zalopay.admin.query.promotiontool;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotiontool.Role;

@Repository
public interface RoleQueryService extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {
}
