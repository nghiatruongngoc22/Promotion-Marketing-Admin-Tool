package vn.zalopay.admin.query.promotiontool;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotiontool.Merchant;

@Repository
public interface MerchantQueryService extends JpaRepository<Merchant, Integer>, JpaSpecificationExecutor<Merchant> {
}
