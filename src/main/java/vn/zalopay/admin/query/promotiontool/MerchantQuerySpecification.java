package vn.zalopay.admin.query.promotiontool;

import vn.zalopay.admin.model.promotiontool.Merchant;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import vn.zalopay.admin.query.AbstractQuerySpecification;

public class MerchantQuerySpecification extends AbstractQuerySpecification<Merchant> {

    private static final Map<String, String> ATTRIBUTE_MAP = new HashMap<String, String>() {{
        put("name", "name");
        put("code", "code");
    }};

    public MerchantQuerySpecification(List<String> filters) {
        super(filters);
    }

    // Method override
    // -----------------------------------------------------------------------

    @Override
    protected void buildOrder(Root<Merchant> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
        if (criteriaQuery.getResultType() != Long.class) {
            criteriaQuery.orderBy(builder.desc(root.get("id")));
        }
    }

    @Override
    protected Map<String, String> attributeMap() {
        return ATTRIBUTE_MAP;
    }
}
