package vn.zalopay.admin.query.promotiontool;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotiontool.User;

@Repository
public interface UserQueryService extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
}
