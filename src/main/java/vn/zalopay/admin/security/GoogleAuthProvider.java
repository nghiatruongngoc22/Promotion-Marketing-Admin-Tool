package vn.zalopay.admin.security;

import java.util.ArrayList;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import vn.zalopay.admin.security.uitls.GoogleAuthUtil;

@Component
public class GoogleAuthProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getPrincipal().toString();
		String password = authentication.getCredentials().toString();

		if(GoogleAuthUtil.checkAuthentication(name, password))
                    return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
                return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}