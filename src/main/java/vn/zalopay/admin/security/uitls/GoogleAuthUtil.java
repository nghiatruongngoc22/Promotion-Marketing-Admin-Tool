/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.zalopay.admin.security.uitls;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import vn.zalopay.admin.utils.HttpUtils;

/**
 *
 * @author baolt
 */
public class GoogleAuthUtil {
     public static final String CHECK_URL = "https://security.vng.com.vn/token-gateway/api/verify_otp/";
    public static final String LOGIN_TYPE = "ga";
    
    
    public static boolean checkAuthentication(String username, String code) {
        try {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("username", username));
            param.add(new BasicNameValuePair("code", code));
            param.add(new BasicNameValuePair("type", LOGIN_TYPE));
            HttpClient client = HttpClientBuilder.create().build();
            
            HttpPost postReq = new HttpPost(CHECK_URL);
            postReq.setEntity(new UrlEncodedFormEntity(param, "UTF-8"));

            HttpResponse response = client.execute(postReq);
            StatusLine status = response.getStatusLine();
            int statusCode = status.getStatusCode();

            if (statusCode == 200) {
                String resp = HttpUtils.getResponsString(response);
                JSONObject respObj = new JSONObject(resp);
                return respObj.getString("status").equals("true") ? true : false;
            } else {
                return false;
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GoogleAuthUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoogleAuthUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
