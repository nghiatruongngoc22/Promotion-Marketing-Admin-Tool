/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.zalopay.admin.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;

/**
 *
 * @author baolt
 */
public class HttpUtils {
       public static String getResponsString(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(
                response.getEntity().getContent()));
        StringBuilder content = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            content.append(line);
        }
        return content.toString();

    }
}
