package vn.zalopay.admin.command;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Set;

public class UserMerchantCommand {

    @Data
    public static class NewMerchant {

        private Integer merchantId;

        @NotEmpty
        private Set<String> roles;

        private String permission;

    }
}