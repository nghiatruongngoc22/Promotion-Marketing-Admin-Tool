package vn.zalopay.admin.command;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Set;

public class UserCommand {

    @Data
    public static class NewUser {

        @NotBlank
        private String userName;

        @NotBlank
        private String displayName;

        @Email
        private String email;

        @NotBlank
        private String password;

        @NotBlank
        private String phone;

        private Boolean isActive = true;

        private Integer merchantId;

        @NotEmpty
        private Set<String> roles;

        private String permission;

    }

    @Data
    public static class UpdateUser {

        @NotBlank
        private String userName;

        @NotBlank
        private String displayName;

        @NotBlank
        private String email;

        @NotBlank
        private String password;

        @NotBlank
        private String phone;

        private Boolean isActive = true;

        private Integer merchantId;

        private Set<String> roles;

        private String permission;
    }
}