package vn.zalopay.admin.command;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

public class RoleCommand {

    @Data
    public static class NewRole {

        @NotBlank
        private String name;

        @NotBlank
        private String code;

        private String allows;
        private String denies;

    }

    @Data
    public static class UpdateRole {

        @NotBlank
        private String name;

        @NotBlank
        private String code;

        private String allows;
        private String denies;
    }
}