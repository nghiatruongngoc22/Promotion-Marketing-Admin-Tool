package vn.zalopay.admin.command;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class UserValidationCommand {

    @NotBlank
    private String path;
}