package vn.zalopay.admin.command;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

public class MerchantCommand {

    @Data
    public static class NewMerchant {

        @NotBlank
        private String name;

        @NotBlank
        private String code;

        private Boolean isActive = true;

    }

    @Data
    public static class UpdateMerchant {

        @NotBlank
        private String name;

        @NotBlank
        private String code;

        private Boolean isActive = true;
    }
}