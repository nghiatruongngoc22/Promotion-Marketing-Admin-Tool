//
// JpaCategoryRepository.java
//
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Mar 25, 2015.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotiontool.User;
import vn.zalopay.admin.model.promotiontool.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import org.springframework.beans.factory.annotation.Qualifier;

@Repository
public class JpaUserRepository extends JpaRepository<Integer, User> implements UserRepository {

    @Autowired
    public JpaUserRepository(@Qualifier("promotiontoolEM") EntityManager em) {
        super(em);
    }

    @Override
    protected Class<User> entityType() {
        return User.class;
    }

    @Override
    public void delete(User user) {
        this.em().remove(user);
    }
}