//
// JpaCategoryRepository.java
//
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Mar 25, 2015.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotiontool.Merchant;
import vn.zalopay.admin.model.promotiontool.MerchantRepository;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Qualifier;

@Repository
public class JpaMerchantRepository extends JpaRepository<Integer, Merchant> implements MerchantRepository {

    @Autowired
    
    public JpaMerchantRepository(@Qualifier("promotiontoolEM") EntityManager em) {
        super(em);
    }

    @Override
    protected Class<Merchant> entityType() {
        return Merchant.class;
    }
}