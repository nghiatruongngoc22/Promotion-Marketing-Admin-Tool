package vn.zalopay.admin.persistence;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Abstract JPA repository
 */
public abstract class JpaRepository<K extends Serializable, E>  {

    private EntityManager em;
    
    public JpaRepository() {
        
    }

    public JpaRepository(EntityManager em) {
        this.em = em;
    }

    public void save(E entity) {
        this.em.persist(entity);
    }

    public E get(K id) {
        return this.em.find(entityType(), id);
    }

    public EntityManager em() {
        return this.em;
    }
    
    public void setEm(EntityManager em) {
        this.em = em;
    }

    /**
     * @return Entity class
     */
    protected abstract Class<E> entityType();
}
