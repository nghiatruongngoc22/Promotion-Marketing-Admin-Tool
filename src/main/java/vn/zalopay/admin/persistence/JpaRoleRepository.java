//
// JpaCategoryRepository.java
//
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Mar 25, 2015.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.model.promotiontool.RoleRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Qualifier;

@Repository
public class JpaRoleRepository extends JpaRepository<Integer, Role> implements RoleRepository {
    
    public JpaRoleRepository(@Qualifier("promotiontoolEM")  EntityManager em ) {
        super(em);
    }

    @Override
    protected Class<Role> entityType() {
        return Role.class;
    }

    @Override
    public Role getByCode(String code) {

        Query query = em().createQuery("select r from Role r where r.code = :code");
        query.setParameter("code", code);

        try {
            return (Role) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}