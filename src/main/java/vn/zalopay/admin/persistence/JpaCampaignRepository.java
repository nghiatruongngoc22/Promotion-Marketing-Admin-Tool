/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.zalopay.admin.persistence;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import vn.zalopay.admin.model.promotionadmin.Campaign;
import vn.zalopay.admin.model.promotionadmin.CampaignRepository;

/**
 *
 * @author baolt
 */
@Repository
public class JpaCampaignRepository extends JpaRepository<Integer, Campaign> implements CampaignRepository {

    @Autowired
    public JpaCampaignRepository(@Qualifier("promotionadminEM") EntityManager em) {
        super(em);
    }

    @Override
    protected Class<Campaign> entityType() {
        return Campaign.class;
    }

    @Override
    public List getAll() {
        return this.em().createQuery("SELECT c FROM Campaign c").getResultList();
    }

}
