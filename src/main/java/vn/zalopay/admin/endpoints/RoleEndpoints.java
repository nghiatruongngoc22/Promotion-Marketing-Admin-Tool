package vn.zalopay.admin.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import vn.zalopay.admin.api.RoleApi;
import vn.zalopay.admin.command.RoleCommand;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.query.promotiontool.RoleQueryService;
import vn.zalopay.admin.query.promotiontool.RoleQuerySpecification;
import vn.zalopay.admin.representation.RoleDTO;
import vn.zalopay.admin.sharedkernel.rest.Paginated;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/_internal/roles")
public class RoleEndpoints {

    private RoleApi roleApi;
    private RoleQueryService roleQueryService;

    @Autowired
    public RoleEndpoints(RoleApi roleApi, RoleQueryService roleQueryService) {
        this.roleApi = roleApi;
        this.roleQueryService = roleQueryService;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RoleDTO newRole(@Valid @RequestBody RoleCommand.NewRole cmd) {
        return roleApi.newRole(cmd);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RoleDTO updateRole(@PathVariable Integer id, @Valid @RequestBody RoleCommand.UpdateRole cmd) {
        return roleApi.updateRole(id, cmd);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public RoleDTO getRole(@PathVariable Integer id) {
        return roleApi.getRole(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Paginated<RoleDTO> query(
            @RequestParam(value = "page_number", defaultValue = "1") int pageNumber,
            @RequestParam(value = "page_size", defaultValue = "100") int pageSize,
            @RequestParam(value = "fq", required = false, defaultValue = "") List<String> fq) {

        org.springframework.data.domain.Page<Role> roles = this.roleQueryService.findAll(
                new RoleQuerySpecification(fq),
                new org.springframework.data.domain.PageRequest(pageNumber - 1, pageSize)
        );

        return new Paginated<>(
                roles.getContent()
                        .stream()
                        .map(RoleDTO::from)
                        .collect(Collectors.toList()),
                pageNumber,
                pageSize,
                roles.getTotalElements()
        );
    }
}
