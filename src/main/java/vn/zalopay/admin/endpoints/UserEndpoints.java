package vn.zalopay.admin.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import vn.zalopay.admin.api.UserApi;
import vn.zalopay.admin.command.UserCommand;
import vn.zalopay.admin.command.UserMerchantCommand;
import vn.zalopay.admin.command.UserValidationCommand;
import vn.zalopay.admin.model.promotiontool.User;
import vn.zalopay.admin.query.promotiontool.UserQueryService;
import vn.zalopay.admin.query.promotiontool.UserQuerySpecification;
import vn.zalopay.admin.representation.UserDTO;
import vn.zalopay.admin.representation.UserValidationDTO;
import vn.zalopay.admin.sharedkernel.rest.Paginated;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@RestController
@RequestMapping("/_internal/users")
public class UserEndpoints {

    private UserApi userApi;
    private UserQueryService userQueryService;

    @Autowired
    public UserEndpoints(UserApi userApi, UserQueryService userQueryService) {
        this.userApi = userApi;
        this.userQueryService = userQueryService;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO newUser(@Valid @RequestBody UserCommand.NewUser cmd) {
        return userApi.newUser(cmd);
    }

    @RequestMapping(value = "/{id}/merchants/{merchantId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO updateUser(@PathVariable Integer id,
                              @PathVariable Integer merchantId,
                              @Valid @RequestBody UserCommand.UpdateUser cmd) {
        return userApi.updateUser(id, merchantId, cmd);
    }

    @RequestMapping(value = "/{id}/merchants", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO newMerchant(@PathVariable Integer id, @Valid @RequestBody UserMerchantCommand.NewMerchant cmd) {
        return userApi.newMerchant(id, cmd);
    }

    @RequestMapping(value = "/{id}/merchants/{merchantId}", method = RequestMethod.DELETE)
    public UserDTO removeMerchant(@PathVariable Integer id, @PathVariable Integer merchantId) {
        return userApi.removeMerchant(id, merchantId);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public UserDTO getUserById(@PathVariable Integer id) {
        return userApi.getById(id);
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public Paginated<UserDTO> query(
            @RequestParam(value = "page_number", defaultValue = "1") int pageNumber,
            @RequestParam(value = "page_size", defaultValue = "100") int pageSize,
            @RequestParam(value = "fq", required = false, defaultValue = "") List<String> fq) {

        org.springframework.data.domain.Page<User> users = this.userQueryService.findAll(
                new UserQuerySpecification(fq),
                new org.springframework.data.domain.PageRequest(pageNumber - 1, pageSize)
        );

        return new Paginated<>(
                users.getContent()
                        .stream()
                        .map(UserDTO::from)
                        .collect(Collectors.toList()),
                pageNumber,
                pageSize,
                users.getTotalElements()
        );
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeUser(@PathVariable Integer id) {
         userApi.removeUser(id);
    }

    @RequestMapping(value = "/{id}/merchants/{merchantId}/validation", method = POST)
    public UserValidationDTO validation(@PathVariable Integer id,
                                        @PathVariable Integer merchantId,
                                        @Valid @RequestBody UserValidationCommand cmd) {
        return userApi.validation(id, merchantId, cmd);
    }
}
