package vn.zalopay.admin.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import vn.zalopay.admin.api.MerchantApi;
import vn.zalopay.admin.command.MerchantCommand;
import vn.zalopay.admin.model.promotiontool.Merchant;
import vn.zalopay.admin.query.promotiontool.MerchantQueryService;
import vn.zalopay.admin.query.promotiontool.MerchantQuerySpecification;
import vn.zalopay.admin.representation.MerchantDTO;
import vn.zalopay.admin.sharedkernel.rest.Paginated;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/_internal/merchants")
public class MerchantEndpoints {

    private MerchantApi merchantApi;
    private MerchantQueryService merchantQueryService;

    @Autowired
    public MerchantEndpoints(MerchantApi merchantApi, MerchantQueryService merchantQueryService) {
        this.merchantApi = merchantApi;
        this.merchantQueryService = merchantQueryService;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public MerchantDTO newMerchant(@Valid @RequestBody MerchantCommand.NewMerchant cmd) {
        return merchantApi.newMerchant(cmd);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public MerchantDTO updateMerchant(@PathVariable Integer id, @Valid @RequestBody MerchantCommand.UpdateMerchant cmd) {
        return merchantApi.updateMerchant(id, cmd);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MerchantDTO getMerchant(@PathVariable Integer id) {
        return merchantApi.getMerchant(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Paginated<MerchantDTO> query(
            @RequestParam(value = "page_number", defaultValue = "1") int pageNumber,
            @RequestParam(value = "page_size", defaultValue = "100") int pageSize,
            @RequestParam(value = "fq", required = false, defaultValue = "") List<String> fq) {

        org.springframework.data.domain.Page<Merchant> merchants = this.merchantQueryService.findAll(
                new MerchantQuerySpecification(fq),
                new org.springframework.data.domain.PageRequest(pageNumber - 1, pageSize)
        );

        return new Paginated<>(
                merchants.getContent()
                        .stream()
                        .map(MerchantDTO::from)
                        .collect(Collectors.toList()),
                pageNumber,
                pageSize,
                merchants.getTotalElements()
        );
    }
}
