package vn.zalopay.admin.endpoints;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import vn.zalopay.admin.persistence.JpaCampaignRepository;

@Controller
public class HomeEndpoints {

    @Autowired
    private JpaCampaignRepository res;

    @RequestMapping(
            value = {
                "", "/", "welcome",
                "", "/home", "home",
                "", "/dashboard", "dashboard",
                "", "/pages", "Pages",
                "", "/users", "Users",
                "", "/users/*", "Users",
                "", "/login**", "Login",}
    )
    String index() {
        return "home";
    }

    @RequestMapping("/campaigns")
    ResponseEntity<List> campaignList() {
        List res_= res.getAll();
        return new ResponseEntity<>(res_,HttpStatus.OK);
    }

}
