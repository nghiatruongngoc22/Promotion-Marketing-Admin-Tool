package vn.zalopay.admin.sharedkernel.exception;

import java.util.Date;

public class ErrorDetails {

    private int code;
    private Date timestamp;
    private String message;
    private String details;

    public ErrorDetails(int code, String message, String details) {
        this.code = code;
        this.timestamp = new Date();
        this.message = message;
        this.details = details;
    }

    public int getCode() {
        return code;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }
}
