//
// ErrorCode.java
//
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Dec 15, 2014.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.sharedkernel.exception;

/**
 * Application error codes
 */
public enum ErrorCode {

    BUSINESS_RULE_VIOLATION,

    INVALID_PARAM,

    INPUT_VIOLATE_BUSINESS_RULE,

    UNKNOWN,

    STATE_VIOLATION,

    NOT_FOUND,

    ACCESS_DENIED,

    DUPLICATED
}
