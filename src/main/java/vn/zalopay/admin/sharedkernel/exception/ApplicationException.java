//
// ApplicationException.java
//
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Dec 15, 2014.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.sharedkernel.exception;

/**
 * Application exception
 */
public class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ApplicationException(String message) {
        super(message);
    }

    public ErrorCode errorCode() {
        return ErrorCode.UNKNOWN;
    }
}
