//
// ExceptionAdvice.java
// 
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Mar 19, 2015.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.sharedkernel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Exception handler used when exception is thrown
 */
@ControllerAdvice
public class ServiceExceptionAdvice {

    // ========================================================================
    // Client error (4xx)
    // ========================================================================

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorDetails> onNotFoundException(NotFoundException e) {
        return responseEntityFromError(
                new ErrorDetails(404, e.getMessage(), "notFound " + e.getMessage())
        );
    }

    @ResponseBody
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ErrorDetails> onMethodNotSupported(HttpRequestMethodNotSupportedException e) {
        return responseEntityFromError(
                new ErrorDetails(405, e.getMessage(), "method " + e.getMessage())
        );
    }

    /**
     * Catch HttpClientErrorException and forward response to client
     */
    @ResponseBody
    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity onHttpClientErrorException(HttpClientErrorException ex) {
        return new ResponseEntity<>(
                ex.getResponseBodyAsString(),
                ex.getResponseHeaders(),
                ex.getStatusCode());
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetails> onUnknownException(Exception ex) {
        return responseEntityFromError(
                new ErrorDetails(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        "Oops, something wrong with the server",
                        "serverError " + ex.getMessage()));
    }

    private ResponseEntity<ErrorDetails> responseEntityFromError(ErrorDetails error) {
        return new ResponseEntity<>(error, HttpStatus.valueOf(error.getCode()));
    }

}
