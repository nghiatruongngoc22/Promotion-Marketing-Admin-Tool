//
// NotFoundException.java
//
// Created by Quyet. Nguyen Minh <quyetnm@vng.com.vn> on Feb 11, 2015.
// Do not copy or use this source code without owner permission
//
package vn.zalopay.admin.sharedkernel.exception;

/**
 * Exception thrown when request resource is not found
 */
public class NotFoundException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    /**
     * @param message Detail message
     */
    public NotFoundException(String message) {
        super(message);
    }

    @Override
    public ErrorCode errorCode() {
        return ErrorCode.NOT_FOUND;
    }

}
