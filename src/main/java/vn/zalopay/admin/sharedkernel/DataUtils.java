package vn.zalopay.admin.sharedkernel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class DataUtils {
  private static ObjectMapper objectMapper = new ObjectMapper();

  private static ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public static <T> T parseData(String content, TypeReference valueTypeRef) {
    try {
      return getObjectMapper().readValue(content, valueTypeRef);
    } catch (IOException e) {
      return null;
    }
  }

  public static <T> T parseData(String content, Class<T> valueType) {
    try {
      return getObjectMapper().readValue(content, valueType);
    } catch (IOException e) {
      return null;
    }
  }

  public static String writeValueAsString(Object value) {
    try {
      return getObjectMapper().writeValueAsString(value);
    } catch (JsonProcessingException e) {
      return null;
    }
  }
}