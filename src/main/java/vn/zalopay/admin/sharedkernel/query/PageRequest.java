package vn.zalopay.admin.sharedkernel.query;

import java.io.Serializable;

/**
 * Paging request
 *
 */
public class PageRequest implements Serializable {

    private static final long serialVersionUID = -4541509938956089562L;

    /**
     * Current page (zero-based index)
     */
    private final long page;
    private final long size;

    /**
     * Creates a new {@link PageRequest} with sort parameters applied.
     *
     * @param page 1-based page index.
     * @param size the size of the page to be returned.*
     */

    public PageRequest(long page, long size) {
        if (page < 1) {
            throw new IllegalArgumentException("Page index must not be less than zero!");
        }

        if (size < 1) {
            throw new IllegalArgumentException("Page size must not be less than one!");
        }

        this.page = page - 1;
        this.size = size;
    }

    public PageRequest next() {
        return new PageRequest(pageNumber() + 1, pageSize());
    }

    public PageRequest previous() {
        return pageNumber() == 0
                ? this
                : new PageRequest(pageNumber() - 1, pageSize());
    }

    public PageRequest first() {
        return new PageRequest(0, pageSize());
    }

    public long pageSize() {
        return size;
    }

    public long pageNumber() {
        return page + 1;
    }

    public long offset() {
        return page * size;
    }

    public boolean hasPrevious() {
        return page > 0;
    }


    public PageRequest previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PageRequest)) {
            return false;
        }

        PageRequest that = (PageRequest) obj;

        return this.page == that.page && this.size == that.size;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = (int) (prime * result + page);
        result = (int) (prime * result + size);

        return 31 * result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Page request [number: %d, size %d]", pageNumber(), pageSize());
    }
}
