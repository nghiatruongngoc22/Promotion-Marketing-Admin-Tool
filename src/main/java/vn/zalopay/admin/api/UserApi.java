package vn.zalopay.admin.api;

import vn.zalopay.admin.model.promotiontool.Merchant;
import vn.zalopay.admin.model.promotiontool.MerchantRepository;
import vn.zalopay.admin.model.promotiontool.RoleRepository;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.model.promotiontool.User;
import vn.zalopay.admin.model.promotiontool.UserMerchant;
import vn.zalopay.admin.model.promotiontool.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.zalopay.admin.command.UserCommand;
import vn.zalopay.admin.command.UserMerchantCommand;
import vn.zalopay.admin.command.UserValidationCommand;
import vn.zalopay.admin.representation.UserDTO;
import vn.zalopay.admin.representation.UserValidationDTO;
import vn.zalopay.admin.sharedkernel.DataUtils;
import vn.zalopay.admin.sharedkernel.exception.NotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserApi {

    private final UserRepository userRepository;
    private final MerchantRepository merchantRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserApi(UserRepository userRepository,
                   MerchantRepository merchantRepository,
                   RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.merchantRepository = merchantRepository;
        this.roleRepository = roleRepository;
    }

    public UserValidationDTO validation(Integer id, Integer merchantId, UserValidationCommand cmd) {

        User user = this.userRepository.get(id);
        if (user == null) {
            throw new NotFoundException("User "+ id +" does not exists ");
        }

        UserMerchant userMerchant = user.getMerchant(merchantId);
        if (userMerchant == null) {
            throw new NotFoundException("User merchant "+ merchantId +" does not exists ");
        }

        return userMerchant.checkAllowAccessAndReturnData(cmd.getPath());

    }

    @Transactional
    public UserDTO newUser(UserCommand.NewUser cmd) {

        Merchant merchant = this.merchantRepository.get(cmd.getMerchantId());
        if (merchant == null) {
            throw new NotFoundException("Merchant "+ cmd.getMerchantId() +" does not exists ");
        }

        Set<Role> roles = this.getRoles(cmd.getRoles());

        UserMerchant userMerchant = new UserMerchant(
                DataUtils.writeValueAsString(roles),
                cmd.getPermission(),
                merchant);

        User user = new User(
                cmd.getUserName(),
                cmd.getDisplayName(),
                cmd.getEmail(),
                cmd.getPassword(),
                cmd.getPhone(),
                cmd.getIsActive(),
                Collections.singletonList(userMerchant));

        this.userRepository.save(user);

        return UserDTO.from(user);
    }

    @Transactional
    public UserDTO updateUser(Integer id, Integer merchantId, UserCommand.UpdateUser cmd) {

        User user = this.userRepository.get(id);
        if (user == null) {
            throw new IllegalArgumentException("User "+ id +" does not exists ");
        }

        UserMerchant userMerchant = user.getMerchant(merchantId);
        if (userMerchant == null) {
            throw new NotFoundException("User merchant "+ merchantId +" does not exists ");
        }

        if (cmd.getMerchantId() != null) {
            Merchant merchant = this.merchantRepository.get(cmd.getMerchantId());
            if (merchant == null) {
                throw new NotFoundException("Merchant "+ cmd.getMerchantId() +" does not exists ");
            }
            userMerchant.setMerchant(merchant);
        }

        user.updateBasicInfo(
                cmd.getUserName(),
                cmd.getDisplayName(),
                cmd.getEmail(),
                cmd.getPassword(),
                cmd.getPhone(),
                cmd.getIsActive());

        Set<Role> roles = this.getRoles(cmd.getRoles());
        userMerchant.update(DataUtils.writeValueAsString(roles), cmd.getPermission());

        this.userRepository.save(user);

        return UserDTO.from(user);
    }

    @Transactional
    public UserDTO newMerchant(Integer id, UserMerchantCommand.NewMerchant cmd) {

        User user = this.userRepository.get(id);
        if (user == null) {
            throw new NotFoundException("User "+ id +" does not exists ");
        }

        Merchant merchant = this.merchantRepository.get(cmd.getMerchantId());
        if (merchant == null) {
            throw new NotFoundException("Merchant "+ cmd.getMerchantId() +" does not exists ");
        }

        Set<Role> roles = this.getRoles(cmd.getRoles());

        UserMerchant userMerchant = new UserMerchant(
                DataUtils.writeValueAsString(roles),
                cmd.getPermission(),
                merchant);
        user.addMerchant(Collections.singletonList(userMerchant));

        this.userRepository.save(user);

        return UserDTO.from(user);
    }

    @Transactional
    public void removeUser(Integer id) {

        User user = this.userRepository.get(id);
        if (user == null) {
            throw new NotFoundException("User "+ id +" does not exists ");
        }

        this.userRepository.delete(user);
    }

    @Transactional
    public UserDTO removeMerchant(Integer id, Integer merchantId) {

        User user = this.userRepository.get(id);
        if (user == null) {
            throw new NotFoundException("User "+ id +" does not exists ");
        }

        user.removeMerchant(merchantId);

        this.userRepository.save(user);

        return UserDTO.from(user);
    }


    @Transactional(readOnly = true)
    public UserDTO getById(Integer id) {

        User user = this.userRepository.get(id);
        if (user == null) {
            throw new NotFoundException("User "+ id +" does not exists ");
        }

        return UserDTO.from(user);
    }

    private Set<Role> getRoles(Set<String> roleCodes) {

        Set<Role> roles = new HashSet<>();

        roleCodes.forEach(code -> {
            Role role = this.roleRepository.getByCode(code);
            if (role == null) {
                throw new NotFoundException("Role "+ code +" does not exists ");
            }
            roles.add(role);
        });

        return roles;
    }

}
