package vn.zalopay.admin.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.zalopay.admin.command.MerchantCommand;
import vn.zalopay.admin.model.promotiontool.Merchant;
import vn.zalopay.admin.model.promotiontool.MerchantRepository;
import vn.zalopay.admin.representation.MerchantDTO;
import vn.zalopay.admin.sharedkernel.exception.NotFoundException;

@Service
public class MerchantApi {

    private final MerchantRepository merchantRepository;

    @Autowired
    public MerchantApi(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    @Transactional
    public MerchantDTO newMerchant(MerchantCommand.NewMerchant cmd) {

        Merchant merchant = new Merchant(cmd.getName(), cmd.getCode(), cmd.getIsActive());

        this.merchantRepository.save(merchant);

        return MerchantDTO.from(merchant);
    }

    @Transactional
    public MerchantDTO updateMerchant(Integer id, MerchantCommand.UpdateMerchant cmd) {

        Merchant merchant = this.merchantRepository.get(id);
        if (merchant == null) {
            throw new NotFoundException("Merchant "+ id +" does not exists ");
        }

        merchant.update(cmd.getName(), cmd.getCode(), cmd.getIsActive());

        this.merchantRepository.save(merchant);

        return MerchantDTO.from(merchant);
    }

    @Transactional(readOnly = true)
    public MerchantDTO getMerchant(Integer id) {

        Merchant merchant = this.merchantRepository.get(id);
        if (merchant == null) {
            throw new NotFoundException("Merchant "+ id +" does not exists ");
        }

        return MerchantDTO.from(merchant);
    }
}
