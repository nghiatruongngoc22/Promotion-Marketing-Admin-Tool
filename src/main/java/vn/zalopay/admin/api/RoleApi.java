package vn.zalopay.admin.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.zalopay.admin.command.RoleCommand;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.model.promotiontool.RoleRepository;
import vn.zalopay.admin.representation.RoleDTO;
import vn.zalopay.admin.sharedkernel.exception.NotFoundException;

@Service
public class RoleApi {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleApi(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Transactional
    public RoleDTO newRole(RoleCommand.NewRole cmd) {

        Role role = new Role(cmd.getName(), cmd.getCode(), cmd.getAllows(), cmd.getDenies());

        this.roleRepository.save(role);

        return RoleDTO.from(role);
    }

    @Transactional
    public RoleDTO updateRole(Integer id, RoleCommand.UpdateRole cmd) {

        Role role = this.roleRepository.get(id);
        if (role == null) {
            throw new NotFoundException("Role "+ id +" does not exists ");
        }

        role.updateRole(cmd.getName(), cmd.getCode(), cmd.getAllows(), cmd.getDenies());

        this.roleRepository.save(role);

        return RoleDTO.from(role);
    }

    @Transactional(readOnly = true)
    public RoleDTO getRole(Integer id) {

        Role role = this.roleRepository.get(id);
        if (role == null) {
            throw new NotFoundException("Role "+ id +" does not exists ");
        }

        return RoleDTO.from(role);
    }
}
