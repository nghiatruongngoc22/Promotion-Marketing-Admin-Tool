package vn.zalopay.admin.representation;

import lombok.Data;
import vn.zalopay.admin.model.promotiontool.Merchant;

import java.text.SimpleDateFormat;

@Data
public class MerchantDTO {

    private Integer id;
    private String name;
    private String code;
    private Boolean isActive;
    private String createdDate;

    public static MerchantDTO from(Merchant merchant) {
        MerchantDTO dto = new MerchantDTO();
        dto.id = merchant.getId();
        dto.name = merchant.getName();
        dto.code = merchant.getCode();
        dto.isActive = merchant.getActive();
        dto.createdDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(merchant.getCreatedDate());
        return dto;
    }
}
