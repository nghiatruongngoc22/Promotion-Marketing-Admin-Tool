package vn.zalopay.admin.representation;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.Data;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.model.promotiontool.UserMerchant;
import vn.zalopay.admin.sharedkernel.DataUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class UserValidationDTO {

    private Boolean isValid;
    private Set<String> allowPaths;
    private Set<String> deniPaths;

    public static UserValidationDTO from(Boolean isValid, Set<String> allowPaths, Set<String> deniPaths) {
        UserValidationDTO dto = new UserValidationDTO();
        dto.isValid = isValid;
        dto.allowPaths = allowPaths;
        dto.deniPaths = deniPaths;
        return dto;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
