package vn.zalopay.admin.representation;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.Data;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.model.promotiontool.UserMerchant;
import vn.zalopay.admin.sharedkernel.DataUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
public class UserMerchantDTO {

    private MerchantDTO merchant;
    private List<RoleDTO> roles;
    private String permission;

    public static UserMerchantDTO from(UserMerchant userMerchant) {
        UserMerchantDTO dto = new UserMerchantDTO();
        dto.merchant = MerchantDTO.from(userMerchant.getMerchant());
        List<Role> roles = DataUtils.parseData(userMerchant.getRole(), new TypeReference<List<Role>>(){} ) ;
        if (roles != null) {
            dto.roles = RoleDTO.from(roles);
        }
        dto.permission = userMerchant.getPermission();
        return dto;
    }

    public static List<UserMerchantDTO> from(Collection<UserMerchant> userMerchants) {
        if (userMerchants.isEmpty()) {
            return null;
        }
        return userMerchants.stream().map(UserMerchantDTO::from).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
