package vn.zalopay.admin.representation;

import lombok.Data;
import vn.zalopay.admin.model.promotiontool.User;

import java.text.SimpleDateFormat;
import java.util.List;

@Data
public class UserDTO {

    private Integer id;
    private String userName;
    private String displayName;
    private String email;
    private String phone;
    private Boolean isActive;
    private String createdDate;
    private List<UserMerchantDTO> merchants;

    public static UserDTO from(User user) {
        UserDTO dto = new UserDTO();
        dto.id = user.getId();
        dto.userName = user.getUserName();
        dto.displayName = user.getDisplayName();
        dto.email = user.getEmail();
        dto.phone = user.getPhone();
        dto.isActive = user.getActive();
        dto.createdDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(user.getCreatedDate());
        dto.merchants = UserMerchantDTO.from(user.merchants());
        return dto;
    }
}
