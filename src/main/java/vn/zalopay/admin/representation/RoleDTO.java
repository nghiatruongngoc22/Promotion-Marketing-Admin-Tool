package vn.zalopay.admin.representation;

import lombok.Data;
import vn.zalopay.admin.model.promotiontool.Role;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class RoleDTO {

    private Integer id;
    private String name;
    private String code;
    private String allows;
    private String denies;

    public static RoleDTO from(Role role) {
        RoleDTO dto = new RoleDTO();
        dto.id = role.getId();
        dto.name = role.getName();
        dto.code = role.getCode();
        dto.allows = role.getAllows();
        dto.denies = role.getDenies();
        return dto;
    }

    public static List<RoleDTO> from(Collection<Role> roles) {
        if (roles.isEmpty()) {
            return null;
        }
        return roles.stream().map(RoleDTO::from).collect(Collectors.toList());
    }
}
