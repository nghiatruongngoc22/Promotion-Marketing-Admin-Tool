/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.zalopay.admin.dao;

import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import vn.zalopay.admin.model.promotiontool.Role;
import vn.zalopay.admin.model.promotiontool.User;

/**
 *
 * @author baolt
 */
@Configuration
@EnableJpaRepositories(basePackages = "vn.zalopay.admin.query.promotionadmin",
        entityManagerFactoryRef = "promotionadminEM",
        transactionManagerRef = "promotionadminTransactionManager")
public class PromotionAdminDBConfiguration {

    @Bean(name = "promotionadminDS")
    @Primary
    @ConfigurationProperties(prefix = "promotionadmin.datasource")
    public DataSource promotionAdminDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("promotionadminEM")
    @Primary
    public LocalContainerEntityManagerFactoryBean promotionadminEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(promotionAdminDataSource());
        factoryBean.setPackagesToScan("vn.zalopay.admin.model.promotionadmin");
        factoryBean.setPersistenceUnitName("promotionadminUnit");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        return factoryBean;
    }

    @Bean("promotionadminTransactionManager")
    public PlatformTransactionManager promotionadminTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(promotionadminEntityManagerFactory().getObject());
        return transactionManager;
    }

}
