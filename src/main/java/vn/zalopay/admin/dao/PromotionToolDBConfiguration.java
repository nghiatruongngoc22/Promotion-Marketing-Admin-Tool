package vn.zalopay.admin.dao;

import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import vn.zalopay.admin.model.promotiontool.Merchant;

/**
 *
 * @author baolt
 */
@Configuration
@EnableJpaRepositories(basePackages = "vn.zalopay.admin.query.promotiontool",
        entityManagerFactoryRef = "promotiontoolEM",
                transactionManagerRef = "promotiontoolTransactionManager")
public class PromotionToolDBConfiguration{

    @Bean(name = "promotiontoolDS")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource promotiontoolDataSource1() {
        return DataSourceBuilder.create().build();
    }

    @Bean("promotiontoolEM")
    public LocalContainerEntityManagerFactoryBean promotiontoolEntityManagerFactory() {
                LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(promotiontoolDataSource1());
        factoryBean.setPackagesToScan("vn.zalopay.admin.model.promotiontool");
        factoryBean.setPersistenceUnitName("promotiontoolUnit");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        return factoryBean;

    }
    
    @Bean("promotiontoolTransactionManager")
    @Primary
    public PlatformTransactionManager promotiontoolTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(promotiontoolEntityManagerFactory().getObject());
        return transactionManager;
    }

}