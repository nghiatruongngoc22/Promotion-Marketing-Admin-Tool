import {Route,Redirect,withRouter} from "react-router-dom"
import React from "react"
import AuthenStore from "store/AuthenticationStore.js"
import {observer} from "mobx-react"

const PrivateRoute= observer((props)=>{
    var Component = props.component;
    var isAuthenticated = AuthenStore.isAuthenticated;
    return(
        <Route path={props.path} component={(props) => (
            !isAuthenticated ? (<Redirect to="/login" />) : (<Component {...props}/>)
        )} />
    )
})

export default PrivateRoute