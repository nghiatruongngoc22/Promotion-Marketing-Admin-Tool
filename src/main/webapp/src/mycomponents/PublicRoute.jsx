import {Route,Redirect,withRouter} from "react-router-dom"
import React from "react"
import AuthenStore from "store/AuthenticationStore.js"
import {observer} from "mobx-react"

const PublicRoute=  observer((props)=>{
    var Component = props.component;
    var isAuthenticated = AuthenStore.isAuthenticated;
    return(
        <Route path={props.path} component={(props) => (
            isAuthenticated ? (<Redirect to="/" />) : (<Component {...props}/>)
        )} />
    )
})

export default PublicRoute