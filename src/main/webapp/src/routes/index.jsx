import Pages from "layouts/Pages/Pages.jsx";
import Dashboard from "layouts/Dashboard/Dashboard.jsx";
import Users from "layouts/Users/Users.jsx";
import Login from "views/LoginPage.jsx";

var indexRoutes = [
    { path: "/login", name: "Login", component: Login,isPrivate : false },
    { path: "/pages", name: "Pages", component: Pages,isPrivate : true },
    { path: "/users", name: "Users", component: Users,isPrivate : true },
    { path: "/", name: "Home", component: Dashboard ,isPrivate : true}
];

export default indexRoutes;
