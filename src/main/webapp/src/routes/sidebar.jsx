import CampaignList from "views/CampaignList.jsx";
import HomePage from "views/HomePage.jsx";

var dashRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "design_app",
    component: HomePage
  },
  {
    path: "/campaginlist",
    name: "Campagin List",
    icon: "design_bullet-list-67",
    component: CampaignList
  },
  { redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashRoutes;
