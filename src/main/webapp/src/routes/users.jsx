import UsersPage from "views/Users/UsersPage.jsx";

const usersRoutes = [
  {
    path: "/users",
    name: "All Users Page",
    short: "UsersPage",
    mini: "RP",
    icon: "tech_mobile",
    component: UsersPage
  }
];

export default usersRoutes;
