import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch,withRouter } from "react-router-dom";
import { observer } from "mobx-react"
import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/now-ui-dashboard.css?v=1.1.1";
import "assets/css/demo.css";
import indexRoutes from "routes/index.jsx";
import PrivateRoute from "mycomponents/PrivateRoute.jsx"
import PublicRoute from "mycomponents/PublicRoute.jsx"


const hist = createBrowserHistory();

const MainApp = observer((props) => {
  return (
    <Switch>
      {indexRoutes.map((prop, key) => {
        if(!prop.isPrivate){
          return <PublicRoute path={prop.path} key={key} component={prop.component} />;
        }else{
          return <PrivateRoute path={prop.path} key={key} component={prop.component} />;
        }
      })}
    </Switch>
  )
})

const MainAppRouter = withRouter(MainApp)

ReactDOM.render(
  <Router history={hist}>
    <MainAppRouter/>
  </Router>,
  document.getElementById("root")
);
