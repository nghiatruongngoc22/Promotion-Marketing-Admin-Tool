
import AuthenStore from "store/AuthenticationStore.js"

const loginURL = "/login"


export function LoginAPI(info) {
    return fetch(loginURL, {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(info)
    }).then(response => {
        if (!response.ok)
            throw new Error(response.statusText)
        return response.json().then((val)=>{
            if(val.status){
                var token =  response.headers.get('authorization').replace("Bearer ","")
                AuthenStore.setJWT(token)
            } else{
                throw new Error("Sai domainname hoac password")
            }
        })
    })
}