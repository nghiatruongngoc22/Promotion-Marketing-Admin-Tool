import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import { Route, Switch, Redirect } from "react-router-dom";

import {Footer, Header, Sidebar} from "components";

import usersRoutes from "routes/users.jsx";

var ps;

class Users extends React.Component {

  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel);
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }

  render() {
      return (
          <div className="wrapper">
              <Sidebar {...this.props} routes={usersRoutes} />
              <div className="main-panel" ref="mainPanel">
                  <Header {...this.props} />
                  <Switch>
                      {usersRoutes.map((prop, key) => {
                          if (prop.redirect)
                              return (
                                  <Redirect from={prop.path} to={prop.pathTo} key={key} />
                              );
                          return (
                              <Route
                                  path={prop.path}
                                  component={prop.component}
                                  key={key}
                              />
                          );
                      })}
                  </Switch>
                  <Footer fluid />
              </div>
          </div>
      );
  }
}

export default Users;
