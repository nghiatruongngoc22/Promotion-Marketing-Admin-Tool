import React from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardFooter,
    Form,
    Container,
    Col,
    Input,
    InputGroup,
    InputGroupAddon
} from "reactstrap";

import { Button } from "components";

import nowLogo from "assets/img/now-logo.png";

import bgImage from "assets/img/bg14.jpg";

import NotificationAlert from 'react-notification-alert';


import { LoginAPI } from "api/AuthenticationAPI.js"



class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isloginning: false,
            username: "",
            password: ""
        };
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e) {
        var name = e.target.name;
        var val = e.target.value;
        var obj = {};
        obj[name] = val;
        this.setState(obj);
    }

    onSubmit(e) {
        e.preventDefault();
        this.setState({
            isloginning: true
        })
        var { username, password } = this.state;
        LoginAPI({ username, password }).catch((err) => {
            console.log(err.message);
            this.refs.notificationAlert.notificationAlert({
                place: "tc",
                message: err.message,
                type: "danger",
                icon: "now-ui-icons ui-1_bell-53",
                autoDismiss: 10
            });
            this.setState({
                isloginning: false
            })
        })
    }

    render() {
        return (
            <div className="wrapper wrapper-full-page" ref="fullPages">
                <div className="full-page section-image">
                    <div>
                        <NotificationAlert ref="notificationAlert" />
                        <div className="full-page-content" >
                            <div className="login-page">
                                <Container>
                                    <Col xs={12} md={8} lg={4} className="ml-auto mr-auto">
                                        <Form onSubmit={this.onSubmit} id="login_form">
                                            <Card className="card-login card-plain">
                                                <CardHeader>
                                                    <div className="logo-container">
                                                        <img src={nowLogo} alt="now-logo" />
                                                    </div>
                                                </CardHeader>
                                                <CardBody>
                                                    <InputGroup
                                                        size="lg"
                                                        className={
                                                            "no-border " +
                                                            (this.state.domainnameFocus ? "input-group-focus" : "")
                                                        }
                                                    >
                                                        <InputGroupAddon>
                                                            <i className="now-ui-icons users_circle-08" />
                                                        </InputGroupAddon>
                                                        <Input
                                                            name="username"
                                                            value={this.state.username}
                                                            type="text"
                                                            placeholder="Domain Name"
                                                            onFocus={e => this.setState({ domainnameFocus: true })}
                                                            onBlur={e => this.setState({ domainnameFocus: false })}
                                                            onChange={this.onChange}
                                                        />
                                                    </InputGroup>
                                                    <InputGroup
                                                        size="lg"
                                                        className={
                                                            "no-border " +
                                                            (this.state.passwordFocus ? "input-group-focus" : "")
                                                        }
                                                    >
                                                        <InputGroupAddon>
                                                            <i className="now-ui-icons ui-1_lock-circle-open" />
                                                        </InputGroupAddon>
                                                        <Input
                                                            name="password"
                                                            value={this.state.password}
                                                            type="password"
                                                            placeholder="Password"
                                                            onFocus={e => this.setState({ passwordFocus: true })}
                                                            onBlur={e => this.setState({ passwordFocus: false })}
                                                            onChange={this.onChange}
                                                        />
                                                    </InputGroup>
                                                </CardBody>
                                                <CardFooter>
                                                    {this.state.isloginning ?
                                                        <Button
                                                            color="primary"
                                                            size="lg"
                                                            block
                                                            round
                                                            form="login_form"
                                                            type="submit"
                                                            disabled
                                                        >
                                                        <i className="now-ui-icons loader_refresh"></i>Login
                                                        </Button> 
                                                        :
                                                        <Button
                                                            color="primary"
                                                            size="lg"
                                                            block
                                                            round
                                                            form="login_form"
                                                            type="submit"
                                                        >
                                                            Login
                                                        </Button>
                                                    }

                                                </CardFooter>
                                            </Card>
                                        </Form>
                                    </Col>
                                </Container>
                            </div>

                        </div>
                        <div
                            className="full-page-background"
                            style={{ backgroundImage: "url(" + bgImage + ")" }}
                        > </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default LoginPage;
