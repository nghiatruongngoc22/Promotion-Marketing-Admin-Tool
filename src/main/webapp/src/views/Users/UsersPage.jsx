import React from "react";
import axios from 'axios';

import {
    Card,
    CardBody,
    CardHeader,
    CardTitle,
    Table,
    Row,
    Col
} from "reactstrap";

import { PanelHeader } from "components";

class UsersPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            users: []
        };
    }

    //the API invoked after React renders a component in the DOM
    componentDidMount() {
        axios.get(`/_internal/users`)
            .then(res => {
                this.setState({
                    users: res.data.items
                });
            })
            .finally(() => {
                console.log(this.state.users);
            })
            .catch(reason => console.log(reason));
    }

    render() {
        return (
            <div>
                <PanelHeader size="sm" />
                <div className="content" style={{marginTop : '0px'}}>
                    <Row>
                        <Col xs={12}>
                            <Card>
                                <CardHeader>
                                    <CardTitle>User</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive>
                                        <thead className="text-primary">
                                            <tr>
                                                <th>UserName</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        {this.state.users.map(user =>
                                            <tr>
                                                <td>{user.userName}</td>
                                                <td>{user.email}</td>
                                                <td>{user.phone}</td>
                                            </tr>
                                        )}
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default UsersPage;
