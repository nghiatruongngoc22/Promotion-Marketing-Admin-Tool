import JWT from "jsonwebtoken"
import { observable, action, decorate, computed } from "mobx";

class AuthenticationStore {
    payload;
    jwt;

    constructor() {
        this.jwt = null;
        this.payload = null;

        console.log(process.env.KEY_ENCRYPT)

        var token = localStorage.getItem('token');
        if (token) {
            var payload = JWT.decode(token);;
            if (payload == null || payload.exp < (Date.now()/1000)) {
                return localStorage.clear();
            }
            this.jwt = token;
            this.payload = payload;
        }
    }

    setJWT(token) {
        this.jwt = token;
        this.payload = JWT.decode(token);
        localStorage.setItem('token', token)
    }

    logout() {
        this.jwt = null;
        this.payload = null;
        localStorage.clear();
    }

    get isAuthenticated(){
        return this.jwt != null && localStorage.getItem('token') != null
    }

    get username(){
        return this.payload.sub;
    }
}

decorate(AuthenticationStore, {
    payload: observable,
    jwt: observable,
    setJWT: action.bound,
    logout : action.bound,
    isAuthenticated : computed,
    username : computed
})

const authenStore = new AuthenticationStore();


export default authenStore;